import 'package:flutter/material.dart';

class ThemeColor {
  static const Color primary = Color(0xff005CAC);

  // text colors
  static const Color text = Color(0xff000000);
  static const Color text2 = Color(0xff282A34);
  static const Color text3 = Color(0xff11223F);
  static const Color placeholder = Colors.grey;
  static const Color placeholder2 = Color(0xffB2B4B7);
  static const Color placeholder3 = Color(0xff8A8A8A);
  static const Color darkGrey500 = Color(0xff666666);
  static const Color darkRed = Color(0xffEC260C);
  static const Color darkGrey = Color(0xff7B7B7B);
  static const Color toolbarGrey = Color(0xffFAFAFA);
  static const Color toolbarGrey2 = Color(0xffF7F7F8);
  static const Color darkRed2 = Color(0xffF43838);
  static const Color darkRed3 = Color(0xffEE2D24);
  static const Color darkRed4 = Color(0xffFF2A2A);
  static Color bgrLeHoiColor = const Color(0xFFFBFBFB);
  static Color bgrTabbarLeHoiColor = const Color(0xFF005CAC);
  static Color unSelectTabbarLeHoiColor = const Color(0xFF999999);

  static const Color grayCalendar = Color(0xffC4C4C4);

  static const Color darkBlack = Color(0xff333333);
  static const Color blue = Color(0xff007AFF);
  static const Color blue2 = Color(0xff3F85FB);

  static const Color blue4 = Color(0xff005EAE);
  static const Color blue3 = Color(0xff142576);
    static const Color blue5 = Color(0xff036AFF);

  // divider
  static const Color dividerGray = Color(0xffCDCED3);
  static const Color darkGrey3 = Color(0xff999999);

  static const Color darkBorderGrey = Color(0xffE3E3E3);
  static const Color darkBorderGrey2 = Color(0xffB2B2B2);

  // background
  static const Color lightGray = Color(0xffF9F9F9);
  static const Color lightRed = Color(0xffFFDCDC);
  static const Color lightGrey = Color(0xffFBFBFB);
  static const Color backgroundGrey = Color(0xffE5E5E5);

  static const Color backgroundGrey2 = Color(0xffF9F9F9);

  static const Color backgroundGrey3 = Color(0xff949494);

  static const Color backgroundGrey4 = Color(0xffe5e5e5);
  static const Color backgroundGrey5 = Color(0xffDDDCDB);

  // static const Color backgroundGrey5 = Color(0xffBABABA);
  static const Color backgroundBlue = Color(0xffD9F2FF);

  static const Color yellowLight = Color(0xffF0C514);
  static const Color greenLight = Color(0xff1AA600);
  static const pink = Color(0xffFFF2F2);
  static const greyToolBar = Color(0xffFAFAFA);

  static const backgroundDefault = Color(0xff8F8F8F);

  static const backgroundToday = Color(0xffECECEC);

  // border
  static const Color green = Color(0xff02B875);
  static const Color redBorder = Color(0xffFFDCDC);
  static const Color darkRedBorder = Color(0xffFF0623);
  static const Color grayBorder = Color(0xffb8b8b8);
  static const Color grayBorder2 = Color(0xffD8D8D8);
  static const Color grayBorder3 = Color(0xffCDCED3);

  // static const Color grayBorder2 = Color(0xffF2F1F1);

  static const Color darkYellow = Color(0xffF5A623);
  static const Color darkBlue = Color(0xff6782B3);
  static const Color blueTitle = Color(0xffF2FAFF);
  static const Color blueBoder = Color(0xff0380ED);

  // button
  static const Color buttonBlue = Color(0xffD1EEFF);

  static const Color buttonBlue2 = Color(0xff0076FF);
  static const Color buttonDisaleMenu = Color(0xff848484);
  static const Color menuBottomBackground = Color(0xffF7F7F8);
  static const Color buttonOrange = Color(0xffDAA634);
  static const Color overLayGreyCalendar = Color(0xffF4F7FD);

  //charts
  static const Color redChartsColor = Color(0xffE86554);

  static const Color yellowChartsColor = Color(0xffEDCA5A);
  static const Color blueChartsColor = Color(0xff6EC4BC);
  static const Color violetChartsColor = Color(0xff8C96F2);
  // Icon
  static const Color iconGrey = Color(0xff989898);
}
