import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// using to define layout space, widget's size, padding, margin, opacity

class ThemeLayouts {
  static double appBarHeight = 46.h;
  static double appBarButtonWidth = 50.w;
  static double bottomNavigationHeight = 56.h;
  static double verticalPadding = 15.w;
  static double horizontalPadding = 16.w;
  static double buttonHeight = 45.h;
  static double buttonRadius = 5.r;
  static double buttonMinWidth = 150.w;
  static double outlinedButtonSide = 1;
  static double disableButtonOpacity = 0.6;
  static EdgeInsets inputContentPadding =
      EdgeInsets.symmetric(vertical: 10.h, horizontal: 15.w);
}
