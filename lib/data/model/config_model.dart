import 'package:api_module_base/data/model/app_update_model.dart';
import 'package:api_module_base/domain/entities/config_entity.dart';

class ConfigModel implements ConfigEntity {
  @override
  String? lvAdInterstitialGapTime;
  @override
  String? adidAdmobNative;
  @override
  String? adidAdmobBanner;
  @override
  String? adidAdmobInterstitial;
  @override
  String? showPopupDatmatkhau;
  @override
  String? nativeSlideBannerPercent;
  @override
  String? homeToplistSlideIcons;
  @override
  String? bannerScreenBuyPro;
  @override
  String? homeFloatButton;
  @override
  String? forceUpdateAppversion;
  @override
  String? minigameGiftEnable;
  @override
  String? enableInappRating;
  @override
  String? disableCreateEventForFree;
  @override
  String? stopVerifyPhoneOtp;
  @override
  String? popupSaleoffLichvietpro;
  @override
  String? settingsShowOnoffTestmode;
  @override
  String? homeFeatureApps;
  @override
  String? showPopupAddloginmethod;
  @override
  String? showPopupAddphone;
  @override
  String? showMoreAds;
  @override
  String? discoveryTopCards;
  @override
  String? adInterstitialNativePercent;
  @override
  String? lichngayNativeadFullPercent;
  @override
  String? syncRequestSystemEvent;
  @override
  String? firstDefaultTab;
  @override
  String? syncRequestQuoteoftheday;
  @override
  AppUpdateModel? appUpdateInfo;
  @override
  String? loginFacebookDisable;
  @override
  String? lvAdInterstitialMaxTimePerSession;
  @override
  String? remindInputBirthday;
  @override
  String? lv9LoginMethod;

  @override
  String? lv9PlaceApiKey;

  ConfigModel({
    this.lvAdInterstitialGapTime,
    this.adidAdmobNative,
    this.adidAdmobBanner,
    this.adidAdmobInterstitial,
    this.showPopupDatmatkhau,
    this.nativeSlideBannerPercent,
    this.homeToplistSlideIcons,
    this.bannerScreenBuyPro,
    this.homeFloatButton,
    this.forceUpdateAppversion,
    this.minigameGiftEnable,
    this.enableInappRating,
    this.disableCreateEventForFree,
    this.stopVerifyPhoneOtp,
    this.popupSaleoffLichvietpro,
    this.settingsShowOnoffTestmode,
    this.homeFeatureApps,
    this.showPopupAddloginmethod,
    this.showPopupAddphone,
    this.showMoreAds,
    this.discoveryTopCards,
    this.adInterstitialNativePercent,
    this.lichngayNativeadFullPercent,
    this.syncRequestSystemEvent,
    this.firstDefaultTab,
    this.syncRequestQuoteoftheday,
    this.appUpdateInfo,
    this.lvAdInterstitialMaxTimePerSession,
    this.remindInputBirthday,
    this.lv9LoginMethod,
  });

  ConfigModel.fromJson(Map<String, dynamic> json) {
    lvAdInterstitialGapTime = json['lv_ad_interstitial_gap_time'];
    adidAdmobNative = json['adid_admob_native'];
    adidAdmobBanner = json['adid_admob_banner'];
    adidAdmobInterstitial = json['adid_admob_interstitial'];
    showPopupDatmatkhau = json['show_popup_datmatkhau'];
    nativeSlideBannerPercent = json['native_slide_banner_percent'];
    homeToplistSlideIcons = json['home_toplist_slide_icons'];
    bannerScreenBuyPro = json['banner_screen_buy_pro'];
    homeFloatButton = json['home_float_button'];
    forceUpdateAppversion = json['force_update_appversion'];
    minigameGiftEnable = json['minigame_gift_enable'];
    enableInappRating = json['enable_inapp_rating'];
    disableCreateEventForFree = json['disable_create_event_for_free'];
    stopVerifyPhoneOtp = json['stop_verify_phone_otp'];
    popupSaleoffLichvietpro = json['popup_saleoff_lichvietpro'];
    settingsShowOnoffTestmode = json['settings_show_onoff_testmode'];
    homeFeatureApps = json['home_feature_apps'];
    showPopupAddloginmethod = json['show_popup_addloginmethod'];
    showPopupAddphone = json['show_popup_addphone'];
    showMoreAds = json['show_more_ads'];
    discoveryTopCards = json['discovery_top_cards'];
    adInterstitialNativePercent = json['ad_interstitial_native_percent'];
    lichngayNativeadFullPercent = json['lichngay_nativead_full_percent'];
    syncRequestSystemEvent = json['sync_request_system_event'];
    firstDefaultTab = json['first_default_tab'];
    syncRequestQuoteoftheday = json['sync_request_quoteoftheday'];
    appUpdateInfo = json['appVersionUpdate'] != null ? AppUpdateModel.fromJson(json['appVersionUpdate']) : null;
    loginFacebookDisable = json['login_facebook_disable'] == null ? '1' : json['login_facebook_disable'].toString();
    lvAdInterstitialMaxTimePerSession = json['lv_ad_interstitial_max_time_per_session']?.toString();
    remindInputBirthday = json['remind_popup_fill_birthday']?.toString();

    lv9PlaceApiKey = (json['lv9_place_api_key'] == null || json['lv9_place_api_key'].toString().isEmpty)
        ? "AIzaSyC2yDpOS84Un6E7pBDcBnoOh9Qs6GTi6UM"
        : json['lv9_place_api_key'];

    lv9LoginMethod =
        (json['lv9_login_method'] == null || json['lv9_login_method'].toString().isEmpty) ? "zalo#google#apple" : json['lv9_login_method'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['lv_ad_interstitial_gap_time'] = lvAdInterstitialGapTime;
    data['adid_admob_native'] = adidAdmobNative;
    data['adid_admob_banner'] = adidAdmobBanner;
    data['adid_admob_interstitial'] = adidAdmobInterstitial;
    data['show_popup_datmatkhau'] = showPopupDatmatkhau;
    data['native_slide_banner_percent'] = nativeSlideBannerPercent;
    data['home_toplist_slide_icons'] = homeToplistSlideIcons;
    data['banner_screen_buy_pro'] = bannerScreenBuyPro;
    data['home_float_button'] = homeFloatButton;
    data['force_update_appversion'] = forceUpdateAppversion;
    data['minigame_gift_enable'] = minigameGiftEnable;
    data['enable_inapp_rating'] = enableInappRating;
    data['disable_create_event_for_free'] = disableCreateEventForFree;
    data['stop_verify_phone_otp'] = stopVerifyPhoneOtp;
    data['popup_saleoff_lichvietpro'] = popupSaleoffLichvietpro;
    data['settings_show_onoff_testmode'] = settingsShowOnoffTestmode;
    data['home_feature_apps'] = homeFeatureApps;
    data['show_popup_addloginmethod'] = showPopupAddloginmethod;
    data['show_popup_addphone'] = showPopupAddphone;
    data['show_more_ads'] = showMoreAds;
    data['discovery_top_cards'] = discoveryTopCards;
    data['ad_interstitial_native_percent'] = adInterstitialNativePercent;
    data['lichngay_nativead_full_percent'] = lichngayNativeadFullPercent;
    data['sync_request_system_event'] = syncRequestSystemEvent;
    data['first_default_tab'] = firstDefaultTab;
    data['sync_request_quoteoftheday'] = syncRequestQuoteoftheday;
    data['appVersionUpdate'] = appUpdateInfo?.toJson();
    data['login_facebook_disable'] = loginFacebookDisable;
    data['lv_ad_interstitial_max_time_per_session'] = lvAdInterstitialMaxTimePerSession;
    data['remind_popup_fill_birthday'] = remindInputBirthday;
    data['lv9_login_method'] = lv9LoginMethod;
    data['lv9_place_api_key'] = lv9PlaceApiKey;
    return data;
  }
}
