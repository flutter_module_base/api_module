class ChannelEndpoint {
  // data
  static const detailDay = 'eventDetailDays';
  static const openNewScreen = 'openNewScreen';
  static const userInfo = 'userInfo';
  static const homeWeather = 'homeWeather';
  static const todayInHistory = 'todayInHistory';
  static const horoscops = 'horoscops';
  static const featureVideo = 'featureVideo';
  static const funFact = 'funFact';
  static const getGioLyThuanPhong = 'getGioLyThuanPhong';
  static const detailDayCanchi = 'detailDayCanchi';
  static const quoteDay = 'quoteDay';
  static const getFloatHome = 'getFloatHome';

  static const getItemList = 'getItemList';

  static const getAppConfig = 'getAppConfig';
  static const getAdsConfig = 'getAdsConfig';
  static const getAppSettings = 'getAppSettings';

  static const getMonthLunar = 'getMonthLunar';
  static const getDayLunar = 'getDayLunar';
  static const getEventDate = 'getEventDate';
  static const getNiceApp = 'getNiceApp';
  static const searchEvent = 'searchEvent';

  static const lunarToSolar = 'lunarToSolar';
  static const solorToLunar = 'solarToLunar';
  static const getErrorCode = 'getErrorCode';
  static const personalEventCount = 'personalEventCount';

  // onclick
  static const onClickAllBenefit = 'onClickAllBenefit';
  static const onClickMoreTodayInHistory = 'onClickMoreTodayInHistory';
  static const onClickHoroscops = 'onClickHoroscrops';
  static const onClickMoreFeatureVideo = 'onClickMoreFeatureVideo';
  static const onClickTodayInHistoryItem = 'onClickTodayInHistoryItem';
  static const onClickVideoItem = 'onClickVideoItem';
  static const onClickWeather = 'onClickWeather';
  static const onClickFunFact = 'onClickFunFact';
  static const onClickGame = 'onClickGame';
  static const onClickNotification = 'onClickNotification';
  static const onClickToNavigateDetailDay = 'onClickToNavigateDetailDay';
  static const onClickToClose = 'onClickToClose';
  static const onClickToShareQuote = 'onClickToShareQuote';
  static const onClickToQuote = 'onClicktoQuote';
  static const onClickFavoriteFunFact = 'onClickFavoriteFunFact';
  static const onClickToOpenDeepLink = 'onClickToOpenDeepLink';
  static const onClickSupport = 'onClickSupport';
  static const onClickSetting = 'onClickSetting';
  static const onClickQrCode = 'onClickQrCode';
  static const onClickTuViList = 'onClickTuViList';
  static const onClickTuViDetail = 'onClickTuViDetail';
  static const onClickLogin = 'onClickLogin';
  static const onClickFriend = 'onClickFriend';
  static const onClickConnect = 'onClickConnect';
  static const onClickEditPhoneNumber = 'onClickEditPhoneNumber';
  static const onClickSetPassword = 'onClickSetPassword';
  static const onClickUpdatePhoneNumber = 'onClickUpdatePhoneNumber';
  static const onClickInappProduct = 'onClickInappProduct';
  static const onClickPremium = 'onClickPremium';
  static const onClickEventNoti = 'onClickEventNoti';
  static const onTapNotification = 'onTapNotification';
  static const onClickThirdPartyLogin = 'onClickThirdPartyLogin';

  static const showAdmob = 'showAdmob';

  // event
  static const onActiveProSuccess = 'onActiveProSuccess';
  static const showBottomBar = 'showBottomBar';
  static const nativeNavigate = 'nativeNavigate';
  static const refreshHome = 'refreshHome';
  static const openChuyenSanTuVi = 'openChuyenSanTuVi';
  static const getSetting = 'getSetting';
  static const setSetting = 'setSetting';
  static const didChangeSetting = 'didChangeSetting';
  static const getDataTotXau = 'getDataNgayTotXau';
  static const openEventDetail = 'openEventDetail';
  static const openMenuEventList = 'openMenuEventList';
  static const unLinkSuccess = 'unLinkSuccess';
  static const deleteAccountSuccess = 'deleteAccountSuccess';
  static const updateUserSuccess = 'updateUserSuccess';
  static const updateUserPremium = 'updateUserPremium';
  static const sharePost = 'sharePost';
  static const openXemNgayTot = 'openXemNgayTot';
  static const logout = 'logout';
  static const didReceiveRemoteNotification = 'didReceiveRemoteNotification';
  static const getApiCommonHeader = 'getApiCommonHeader';
  static const getVersionCode = 'getVersionCode';
  static const getEnvironment = 'getEnvironment';
  static const getIsDebugMode = 'isDebugMode';
  static const didLogin = 'didLogin';
  static const sendFeedback = 'sendFeedback';
  static const appRating = 'appRating';
  static const showAdsReason = 'showAdsReason';
  static const handleServerError = 'handleServerError';
  static const handleDeepLink = 'handleDeepLink';
  static const openLoginScreen = 'openLoginScreen';
  static const permissonLocationSelected = 'permissonLocationSelected';
  static const askLocationPermission = 'askLocationPermission';
  static const askCameraPermission = 'askCameraPermission';
  static const askStoragePermission = 'askStoragePermission';
  static const changeAppState = 'changeAppState';
  static const didShowAdsMobFull = 'didShowAdsMobFull';
  static const getImageFromCamera = 'getImageFromCamera';
  static const getImageFromGallery = 'getImageFromGallery';

  //menu
  static const selectTabBar = 'selectTabBar';

  static const getSelectedTab = 'getSelectTab';

  static const changeSelectedTab = 'changeSelectTab';

  static const eventDidChange = 'eventDidChange';

  static const backScreen = 'backScreen';

  //service pro

  static const onClickGuide = 'onClickGuide';
  static const onClickContact = 'onClickContact';
  static const verifyPurchaseFailed = 'verifyPurchaseFailed';

  //discovery

  static const onClickNiceApp = 'onClickNiceApp';
}
