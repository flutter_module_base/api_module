// abmob_state_rate_provider.dart
const String countStateNativedKey = "count_state_natived";
const String countStateBannerKey = "count_state_banner";
const String countRateFromSeverKey = "count_rate_fromSever";

// app_local_datasource.dart
const guideXemNgayTot = 'guide_xem_ngay_tot';
const firstUsingAppKey = 'first_using_app';
const showPopupDetailDayKey = 'show_popup_detail_day';
const showPopupDayNowKey = 'show_popup_day_now';
const countSession = 'count_session';
const firstTimeUsingApp = 'first_time_using_app';
const timeShowAdsmobFull = 'time_show_adsmob_full';
const firstTimeUsingChiTietNgay = 'first_time_using_chi_tiet_ngay';
const inputBirthdayFlowTimestamp = 'input_birthday_flow_timestamp';
const adMaxTimePerSession = 'ad_max_time_per_session';
const userIdDidShowFlowBirthday = 'user_id_did_show_flow_birthday';
const didShowNotificationPermission = 'did_show_notification_permission';
const didShowLocationPermission = 'did_show_location_permission';

// banner_local_datasource.dart
const bannerHome = 'banner_home';

// config_local_datasource.dart
const config = 'config';

// count_open_detail_calendar_datasource.dart
const countOpenDetailHiepKy = 'countOpenDetailHiepKy';

// family_local_provider.dart
const familyKey = 'family_local_data';

// feedback_local_datasource.dart
const feedbackXemNgayTot = 'feedback_xem_ngay_tot';

// guide_local_datasource.dart
const parentId = 'parentId';
const categoryId = 'categoryId';
const postId = 'postId';

// he_lich_local_datasource.dart
const heLichKey = 'he_lich';

// key_rsa_triplet_provider.dart
const String publicRsaKey = "public_rsa_key";
const String privateRsaKey = "private_rsa_key";
const String identifier = 'identifier';

// key_time_cache_data_provider.dart
const String cacheTimeNgayTotXauKey = "cache_ngay_tot_xau";
const String cacheDetailDayHeHiepKyKey = "cache_detail_day_he_hiep_ky";
const String cacheDetailDayHeNgocHapKey = "cache_detail_day_he_ngoc_hap";

// notification_local_datasource.dart
const notiGroupIdKey = 'noti_group_id';
const notificationsKey = 'notifications';

// purchase_detail_local_provider.dart
const purchaseKey = 'purchase_detail_local_data';

// quote_local_datasource.dart
const quoteDayKey = 'quote_day_key';

// transaction_local_provider.dart
const transactionDateKey = 'transaction_date';

// tu_vi_local_datasource.dart
const tuviKey = 'tuvi';

// viec_local_datasource.dart
const viec = 'viec';
const caseXemNgayTot = 'cases_xem_ngay_tot_theo_viec';

// bat_trach_local_datasource.dart
const xemPhongThuy = 'xem_phong_thuy_v2';
const keyWife = 'key_wife_v2';
const keyHusband = 'key_husband_v2';