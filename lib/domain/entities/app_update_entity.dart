abstract class AppUpdateEntity {
  String? get message;
  int? get updateStatus;
}