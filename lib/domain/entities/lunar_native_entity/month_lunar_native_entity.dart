abstract class MonthLunarNativeEntity {
  int? get month;
  int? get leap;
}
