abstract class DayLunarNativeEntity {
  int? get day;
  int? get dayOfWeek;
}
