abstract class ItemEntity {
  String? get title;
  String? get link;
  String? get icon;
}