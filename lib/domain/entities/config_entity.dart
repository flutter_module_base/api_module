import 'package:api_module_base/domain/entities/app_update_entity.dart';

abstract class ConfigEntity {
  String? get lvAdInterstitialGapTime;
  String? get adidAdmobNative;
  String? get adidAdmobBanner;
  String? get adidAdmobInterstitial;
  String? get showPopupDatmatkhau;
  String? get nativeSlideBannerPercent;
  String? get homeToplistSlideIcons;
  String? get bannerScreenBuyPro;
  String? get homeFloatButton;
  String? get forceUpdateAppversion;
  String? get minigameGiftEnable;
  String? get enableInappRating;
  String? get disableCreateEventForFree;
  String? get stopVerifyPhoneOtp;
  String? get popupSaleoffLichvietpro;
  String? get settingsShowOnoffTestmode;
  String? get homeFeatureApps;
  String? get showPopupAddloginmethod;
  String? get showPopupAddphone;
  String? get showMoreAds;
  String? get discoveryTopCards;
  String? get adInterstitialNativePercent;
  String? get lichngayNativeadFullPercent;
  String? get syncRequestSystemEvent;
  String? get firstDefaultTab;
  String? get syncRequestQuoteoftheday;
  AppUpdateEntity? get appUpdateInfo;
  String? get loginFacebookDisable;
  String? get lvAdInterstitialMaxTimePerSession;
  String? get remindInputBirthday;
  String? get lv9LoginMethod;
  String? lv9PlaceApiKey;
}
