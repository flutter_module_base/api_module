abstract class SignalEntity {
  String? get id;
  String? get signal;
  String? get type;
  String? get userId;
  String? get acessToken;
  String? get syncEvents;
  String? get isLogin;
}