abstract class ErrorEntity {
  String? get id;
  String? get code;
  String? get lang;
  String? get content;
  String? get modifyTime;
}