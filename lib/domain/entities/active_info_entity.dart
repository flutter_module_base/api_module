abstract class ActiveInfoEntity {
  String? get id;
  String? get userId;
  String? get premiumTypeId;
  String? get startTime;
  String? get endTime;
  String? get renewalDate;
  String? get transactionId;
  String? get modifyBy;
  String? get pushRemind;
  String? get premiumGroups;
  String? get isPro;
  String? get placeShowAd;
  String? get thumb;
}
