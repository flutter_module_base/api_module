abstract class FloatButtonEntity {
  String? get show;
  String? get iconImage;
  String? get link;
  String? get showForProUser;
}
