import 'package:api_module_base/domain/repositories/repositories.dart';

class UserUsecases {
  final UserRepository _userRepository;

  UserUsecases(this._userRepository);
}
