import 'package:get_it/get_it.dart';
import 'package:api_module_base/data/repositories/user_repository_impl.dart';
import 'package:api_module_base/domain/repositories/user_repository.dart';

Future<void> repositoryModule(GetIt getIt) async {
  // getIt
  //   ..registerLazySingleton<ApiConfigRepository>(
  //       () => ApiConfigRepositoryImpl(getIt()))
  //   ..registerFactory<PremiumRepository>(() => PremiumRepositoryImpl(getIt()))
  //   ..registerFactory<UserRepository>(() => UserRepositoryImpl(
  //         getIt(),
  //         getIt(),
  //         getIt(),
  //       ))
  //   ..registerFactory<AppRepository>(
  //       () => AppRepositoryImpl(getIt(), getIt(), getIt()))
  //   ..registerFactory<KeyRsaTripletRepository>(
  //       () => KeyRsaTripletRepositoryImpl(
  //             getIt(),
  //           ))
  //   ..registerFactory<LunarCalendarNativeRepository>(
  //       () => LunarCalendarFromNativeImpl(getIt()))
  //   ..registerFactory<CacheVersionRepository>(
  //       () => CacheVersionRepositoryImpl(getIt()));
}
