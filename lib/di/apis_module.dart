import 'dart:async';

import 'package:api_module_base/api_module_base_main.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';

import 'package:get_it/get_it.dart';

import '../core/core.dart';
import 'dart:convert';
import 'dart:io';
import 'package:dio/adapter.dart';
import 'dart:io' as IO;

Future<Dio> _buildDio(ApiConfig config, EventBusHandler sessionEvent, Lock lock) async {
  // final PEM = await rootBundle.loadString('assets/lichviet.pem');
  final _dio = Dio()
    ..options.baseUrl = config.baseUrl
    ..options.contentType = "application/x-www-form-urlencoded; charset=utf-8"
    ..options.receiveDataWhenStatusError
    ..options.followRedirects = false;
  (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (IO.HttpClient client) {
    client.badCertificateCallback = (X509Certificate cert, String host, int port) {
      // if (cert.pem == PEM) {
      //   // Verify the certificate
      //   return true;
      // }
      // return false;
       return true;
    };
    return client;
  };

  // Add interceptors

  _dio.interceptors.addAll([
    logInterceptor,
    AuthInterceptor(),

    // LogInterceptor(responseBody: true),
    // NetworkLoggerInterceptor(),
    ApiModulesBase.getInstance().managerCache!.interceptor,
    SessionInterceptor(
      baseUrl: config.baseUrl,
      lock: lock,
      onLockRequests: () {
        _dio
          ..lock()
          ..interceptors.requestLock.lock()
          ..interceptors.errorLock.lock();
      },
      onUnlockRequests: () {
        _dio
          ..unlock()
          ..interceptors.responseLock.unlock()
          ..interceptors.errorLock.unlock();
      },
      onSessionExpired: () {
        sessionEvent.fire(sessionExpiredEvent);
      },
    ),
  ]);
  if (ApiModulesBase.getInstance().aliceShakeApi != null) {
    _dio.interceptors.add(ApiModulesBase.getInstance().aliceShakeApi!.getDioInterceptor());
  }
  return _dio;
}

Future<void> apisModule(GetIt getIt, ApiConfig config) async {
  getIt.registerLazySingleton<EventBusHandler>(() => EventBusHandler());
  getIt.registerLazySingleton<Lock>(() => Lock());
  Dio dio = await _buildDio(
    config,
    getIt(),
    getIt(),
  );
  getIt
    ..registerLazySingleton<Dio>(
      () => dio,
    )
    ..registerLazySingleton<ApiHandler>(() => ApiHandlerImpl(
          getIt(),
        ));
}
