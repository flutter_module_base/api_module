class DataUtils {
  static bool convertDataBoolean(dynamic value) {
    if (value is String) {
      if (value == '0') {
        return false;
      } else {
        return true;
      }
    }
    if (value is int) {
      if (value == 0) {
        return false;
      } else {
        return true;
      }
    }
    if (value is bool) {
      return value;
    }
    return false;
  }


}
