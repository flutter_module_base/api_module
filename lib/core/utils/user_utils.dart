import 'package:api_module_base/core/utils/format_datetime.dart';
import 'package:api_module_base/domain/entities/user_entity.dart';

class UserUtils {
  static bool checkLogin(UserEntity? userInfo) {
    if (userInfo == null) {
      return false;
    } else if (userInfo.id == null) {
      return false;
    } else {
      return true;
    }
  }
}

class ServiceDate {
  String name;
  String dateTime;
  String icon;
  ServiceDate(this.name, this.dateTime, this.icon);
}
