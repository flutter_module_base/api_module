import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ImageNetworkUtils {
  
  static Image imageNetworkResizeUsingPrecache({
    required String url,
    required double width,
    required double height,
    required double scale,
    required AlignmentGeometry alignment,
    required BoxFit? fit,
    required Widget? errorWidget,
    required Widget? placeholderWidget,
    bool bestFit = false,
  }) {
    return Image(
      image: CachedNetworkImageProvider(url,),
      width: width,
      height: height,
      alignment: alignment,
      errorBuilder: (context, _, __) =>
          errorWidget ??
          Center(
            child: SizedBox(
              width: 40.w,
              height: 40.w,
              child: const Icon(Icons.error),
            ),
          ),
      loadingBuilder: (context, child, loadingProgress) {
        if (loadingProgress == null) {
          return child;
        }
        return Center(
          child: placeholderWidget,
        );
      },
      frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
        return child;
      },
      fit: fit,
    );
  }
}
