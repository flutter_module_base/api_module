import 'package:dio/dio.dart';
import 'package:flutter/services.dart';


/// Add access_token, more needed params into request's header
class AuthInterceptor extends Interceptor {

 

  AuthInterceptor(
   

  );

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
   
    final Map<String, String> headers = {};
   
    // headers['encrypt'] = RemoteConstants.encrypt;

    // if (GetIt.I<UserCubit>().state.userInfo != null) {
    //   if (GetIt.I<UserCubit>().state.userInfo!.id != null) {
    //     headers['user_id'] =
    //         GetIt.I<UserCubit>().state.userInfo!.id!.toString();
    //   }
    // }
    options.headers = headers;
    return super.onRequest(options, handler);
  }



  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    // if (response.data['status'] == 2) {
    //   return handler.reject(DioError(
    //       requestOptions: response.requestOptions,
    //       error: AuthEmailException()));
    // }
    return handler.next(response);
  }
}
