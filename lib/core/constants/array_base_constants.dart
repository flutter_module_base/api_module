import 'package:api_module_base/core/constants/icon_base_constants.dart';
import 'package:api_module_base/core/constants/image_base_constants.dart';
import 'package:api_module_base/data/model/city_model.dart';

class ArrayBaseConstants {
  String converDateToStringVietNam(DateTime dateTime) {
    String date = '';
    switch (dateTime.weekday) {
      case DateTime.monday:
        date = "T.HAI";
        break;

      case DateTime.tuesday:
        date = "T.BA";
        break;
      case DateTime.wednesday:
        date = "T.TƯ";
        break;
      case DateTime.thursday:
        date = "T.NĂM";
        break;
      case DateTime.friday:
        date = "T.SÁU";
        break;
      case DateTime.saturday:
        date = "T.BẢY";
        break;
      case DateTime.sunday:
        date = "CHỦ NHẬT";
        break;
    }

    return date;
  }

  String converDateToStringVietNam2(DateTime dateTime) {
    String date = '';
    switch (dateTime.weekday) {
      case DateTime.monday:
        date = "T.HAI";
        break;

      case DateTime.tuesday:
        date = "T.BA";
        break;
      case DateTime.wednesday:
        date = "T.TƯ";
        break;
      case DateTime.thursday:
        date = "T.NĂM";
        break;
      case DateTime.friday:
        date = "T.SÁU";
        break;
      case DateTime.saturday:
        date = "T.BẢY";
        break;
      case DateTime.sunday:
        date = "CN";
        break;
    }

    return date;
  }

  List<CityModel> getCityName() {
    List<CityModel> listCityName = [];
    listCityName.add(CityModel(id: -1, name: 'Tự động', cityName: 'Auto'));
    listCityName.add(CityModel(id: 0, name: 'Hồ Chí Minh', cityName: 'Ho Chi Minh'));
    listCityName.add(CityModel(id: 1, name: "Hà Nội", cityName: "Hanoi"));
    listCityName.add(CityModel(id: 2, name: "Đà Nẵng", cityName: "Da Nang"));
    listCityName.add(CityModel(id: 3, name: "Hải Phòng", cityName: "Haiphong"));
    listCityName.add(CityModel(id: 4, name: "An Giang", cityName: "An Giang Province"));
    listCityName.add(CityModel(id: 5, name: "Bà Rịa Vũng Tàu", cityName: "Ba Ria - Vung Tau"));
    listCityName.add(CityModel(id: 6, name: "Bắc Giang", cityName: "Bac Giang"));
    listCityName.add(CityModel(id: 7, name: "Bắc Kạn", cityName: "Bắc Kạn Province"));
    listCityName.add(CityModel(id: 8, name: "Bạc Liêu", cityName: "Bac Lieu"));

    listCityName.add(CityModel(id: 9, name: "Bắc Ninh", cityName: "Bac Ninh Province"));
    listCityName.add(CityModel(id: 10, name: "Bến Tre", cityName: "Ben Tre"));
    listCityName.add(CityModel(id: 11, name: "Bình Dương", cityName: "Binh Duong"));
    listCityName.add(CityModel(id: 12, name: "Bình Phước", cityName: "Binh Phuoc"));
    listCityName.add(CityModel(id: 13, name: "Bình Thuận", cityName: "Binh Thuan"));
    listCityName.add(CityModel(id: 14, name: "Bình Định", cityName: "Bình Định"));
    listCityName.add(CityModel(id: 15, name: "Cà Mau", cityName: "Ca Mau"));
    listCityName.add(CityModel(id: 16, name: "Cần Thơ", cityName: "Can Tho"));
    listCityName.add(CityModel(id: 17, name: "Cao Bằng", cityName: "Cao Bang"));
    listCityName.add(CityModel(id: 18, name: "Gia Lai", cityName: "Gia Lai Province"));

    listCityName.add(CityModel(id: 19, name: "Hà Giang", cityName: "Ha Giang"));
    listCityName.add(CityModel(id: 20, name: "Hà Nam", cityName: "Hà Nam Province"));
    listCityName.add(CityModel(id: 21, name: "Hà Tĩnh", cityName: "Ha Tinh Province"));
    listCityName.add(CityModel(id: 22, name: "Hải Dương", cityName: "Hai Duong"));
    listCityName.add(CityModel(id: 23, name: "Hậu Giang", cityName: "Hau Giang"));
    listCityName.add(CityModel(id: 24, name: "Hoà Bình", cityName: "Hoa Binh"));
    listCityName.add(CityModel(id: 25, name: "Huế", cityName: "Thua Thien Hue"));
    listCityName.add(CityModel(id: 26, name: "Hưng Yên", cityName: "Hung Yen Province"));
    listCityName.add(CityModel(id: 27, name: "Khánh Hoà", cityName: "Khanh Hoa Province"));
    listCityName.add(CityModel(id: 28, name: "Kiên Giang", cityName: "Kien Giang"));
    listCityName.add(CityModel(id: 29, name: "Kon Tum", cityName: "Kon Tum Province"));
    listCityName.add(CityModel(id: 30, name: "Lai Châu", cityName: "Lai Chau"));

    listCityName.add(CityModel(id: 31, name: "Lâm Đồng", cityName: "Lâm Đồng"));
    listCityName.add(CityModel(id: 32, name: "Lạng Sơn", cityName: "Lang Son Province"));
    listCityName.add(CityModel(id: 33, name: "Lào Cai", cityName: "Lao Cai"));
    listCityName.add(CityModel(id: 34, name: "Long An", cityName: "Long An Province"));
    listCityName.add(CityModel(id: 35, name: "Nam Định", cityName: "Nam Dinh"));
    listCityName.add(CityModel(id: 36, name: "Nghệ An", cityName: "Nghe An"));
    listCityName.add(CityModel(id: 37, name: "Ninh Bình", cityName: "Ninh Bình Province"));
    listCityName.add(CityModel(id: 38, name: "Ninh Thuận", cityName: "Ninh Thuan Province"));
    listCityName.add(CityModel(id: 39, name: "Phú Thọ", cityName: "Phu Tho Province"));
    listCityName.add(CityModel(id: 40, name: "Phú Yên", cityName: "Phú Yên Province"));

    listCityName.add(CityModel(id: 41, name: "Quảng Bình", cityName: "Quang Binh Province"));
    listCityName.add(CityModel(id: 42, name: "Quảng Nam", cityName: "Quang Nam Province"));
    listCityName.add(CityModel(id: 43, name: "Quảng Ngãi", cityName: "Quang Ngai"));
    listCityName.add(CityModel(id: 44, name: "Quảng Ninh", cityName: "Quảng Ninh Province"));
    listCityName.add(CityModel(id: 45, name: "Quảng Trị", cityName: "Quảng Trị Province"));
    listCityName.add(CityModel(id: 46, name: "Sóc Trăng", cityName: "Soc Trang"));
    listCityName.add(CityModel(id: 47, name: "Sơn La", cityName: "Son La"));
    listCityName.add(CityModel(id: 48, name: "Tây Ninh", cityName: "Tây Ninh Province"));
    listCityName.add(CityModel(id: 49, name: "Thái Bình", cityName: "Thai Binh"));
    listCityName.add(CityModel(id: 50, name: "Thái Nguyên", cityName: "Thai Nguyen"));

    listCityName.add(CityModel(id: 51, name: "Thanh Hoá", cityName: "Thanh Hoa"));
    listCityName.add(CityModel(id: 52, name: "Tiền Giang", cityName: "Tien Giang"));
    listCityName.add(CityModel(id: 53, name: "Trà Vinh", cityName: "Tra Vinh"));
    listCityName.add(CityModel(id: 54, name: "Tuyên Quang", cityName: "Tuyên Quang Province"));
    listCityName.add(CityModel(id: 55, name: "Vĩnh Long", cityName: "Vinh Long"));
    listCityName.add(CityModel(id: 56, name: "Vĩnh Phúc", cityName: "Vinh Phuc Province"));
    listCityName.add(CityModel(id: 57, name: "Yên Bái", cityName: "Yen Bai Province"));
    listCityName.add(CityModel(id: 58, name: "Đắk Lắk", cityName: "Đắk Lắk Province"));
    listCityName.add(CityModel(id: 59, name: "Đắk Nông", cityName: "Dak Nong"));
    listCityName.add(CityModel(id: 60, name: "Đồng Nai", cityName: "Dong Nai"));

    listCityName.add(CityModel(id: 61, name: "Đồng Tháp", cityName: "Đồng Tháp Province"));
    listCityName.add(CityModel(id: 62, name: "Điện Biên", cityName: "Dien Bien"));

    return listCityName;
  }
}
