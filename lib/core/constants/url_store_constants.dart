class UrlStoreConstants {
  static const urlAppStore = 'http://itunes.apple.com/lookup?bundleId=';
  static const urlGooglePlay = 'https://play.google.com/store/apps/details?id=';
  static const marketAppStore = 'https://apps.apple.com/app/id';
  static const marketGooglePlay = 'market://details?id=';
}