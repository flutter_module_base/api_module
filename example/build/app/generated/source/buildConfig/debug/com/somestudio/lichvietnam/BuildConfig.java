/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.somestudio.lichvietnam;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.somestudio.lichvietnam";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 577;
  public static final String VERSION_NAME = "9.1.0";
}
