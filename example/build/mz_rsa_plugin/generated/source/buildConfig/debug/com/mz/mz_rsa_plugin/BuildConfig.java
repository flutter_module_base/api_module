/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.mz.mz_rsa_plugin;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.mz.mz_rsa_plugin";
  public static final String BUILD_TYPE = "debug";
}
